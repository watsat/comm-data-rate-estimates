# README #

### What is this repository for? ###

* Calculating the approximate data rate and [link margin](https://en.wikipedia.org/wiki/Link_margin) for the communication system.
* The primary script is "datarate.py"

### How do I get set up? ###

* It utilizes some simple tools from the [scientific Python](www.scipy.org) stack.
* Easiest way to get started on Windows is to download [Anaconda](https://www.continuum.io/downloads)
* On Linux, you an get going in the same way, or by installing stuff (Numpy, Scipy, etc...) on the command line.

### About the SciPy stack ###
Python has a variety of scientific computing packages that give it capability similar to Matlab.  All of these tools are free open source software and are used both in industry and academia.  Python is capable of a wide range of number crunching tasks for example simulating dynamical systems, solving differential equations, machine learning and statistics, signal processing and filter design, etc...

This project just uses elementary operations to estimate data rates, so the learning curve should be quite forgiving.

You can get started with Scipy [here](https://www.scipy.org/getting-started.html)

### ECEF coordinates ###
* I've used [ECEF coordinates](http://what-when-how.com/gps-with-high-rate-sensors/ecef-coordinate-systems-gps/), which take into account the oblate spheroidal shape of the earth.  I'm actually not entirely sure if it's being done correctly, particularly what type of assumptions the AGI STK simulation used.  This should probably be double checked and investigated.

### Tasks ###
* Refactor the code to be more user friendly.
* Write some documentation about the link budget calculations
* Create a simple user interface, either a GUI or .config file so that we can enter in parameters and get back information about potential data rates.
* Create some meaningful and pretty plots to show to competition judges/sponsors/fans/etc...
* Make the program abstract enough that we can handle modifications to our modem specifications or other changes.
* Make the program usable for future WatSat members.
* Use Panadas dataframes instead of Numpy arrays.

More advanced functions would be to for example do a Monte Carlo simulation over a bunch of the paramters, sensitivity analysis of the parameters, and some worst case analysis.