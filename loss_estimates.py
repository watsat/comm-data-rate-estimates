from numpy import *

#---SYSTEM GAIN---
Ptx = 30. #Transmitter power dBm
Gsat = 3.5 #Satellite antenna gain dB
Gbs = {'2m, e=0.5' : 20., '3m, e=0.6' : 23., '3.5m, e=0.7' : 27.} #BS ANT dB
Srx = {'Turbo' : 100., 'Fast' : 108., 'Slow' : 116.} #RX sens dB

#---SYSTEM LOSSES---
Lc = 2. #Connector losses dB
Ll = 0.8 #Line losses dB
Lrl = 2. #Reflection (return) losses dB - worst case
LA = 0. #Atmospheric losses dB
Lp = 1.5 #Pointing loss dB
Lpol = 1. #Polarization loss dB (total guess...)
Mfade = 8 #8dB additional margin - not really for fading but general margin

#---FSPL inverse---
c = 3e8 #speed of light, m/s
f = 915e6 #915MHz - system frequency
def max_dist(S, L, G, f):
    '''
    Calculates the maximum propagation distance

    S: System sensitivity in dBm    
    L: Total losses in dB
    G: System gain in dB
    '''
    M = S + G - L #Margin in dB
    M = 10**(M/10.) #Margin as a wattage ratio
    d = c*sqrt(M)/(4*pi*f) #maximum distance in meters (m)
    return d

L = Lc + Ll + Lrl + LA + Lp + Lpol + Mfade

for S in Srx:
    for G in Gbs:
        d = max_dist(Srx[S], L, Gbs[G] + Ptx + Gsat, f)
        print 'Rate: %s, Gbs: %s, d: %dKm' % (S, G, d / 1000.)
