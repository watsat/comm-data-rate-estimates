import numpy as np
import pandas as pd

from itertools import product
from numpy import sin, cos, pi, sqrt, array, dot, arccos, concatenate
from numpy.linalg import norm
from matplotlib import pyplot as plt
from matplotlib.collections import LineCollection
from matplotlib.colors import ListedColormap, BoundaryNorm

#Simulation results at altitudes 600, 700, 800, 900 Km
#O = [pd.read_csv(str('%d00km_orbit.csv' %i), index_col = 'Time (UTCG)')[0:1001]
#     for i in [6]]

#O = [pd.read_csv(str('%d00km_orbit.csv' %i)) for i in [9]]
O = [pd.read_csv(str('%d00km_orbit.csv' %i)) for i in [6]]

dr = pd.date_range(start = O[0].index[0], end = O[0].index[-1], freq = '5s')
#O = [o.reindex(dr).interpolate('cubic') for o in O]

R = 6.3e6 #Radius of earth in Km
a = 6378137 #Earth semi major axis (m)
b = 6356752.314245 #Earth semi minor axis (m)
e = sqrt((a**2 - b**2))/a #eccentricity

def RM(phi): #Meridian ellipse
    return a*(1 - e**2) / (1 - e**2*sin(phi)**2)**(3./2)

def RN(phi): #Normal ellipse
    return a / sqrt(1 - e**2*sin(phi)**2)

def ECEF_x(phi, lmbda, h):
    return (RN(phi) + h)*cos(phi)*cos(lmbda)

def ECEF_y(phi, lmbda, h):
    return (RN(phi) + h)*cos(phi)*sin(lmbda)

def ECEF_z(phi, lmbda, h):
    return (RN(phi)*(1 - e**2) + h)*sin(phi)

#--------------UW location---------------
#Lat long
UW_phi = 43.4668*pi/180
UW_lmbda = -80.5164*pi/180
UW_h = 325 #m

#I haven not double checked the correctness of these equations
#ECEF xyz
UW_x = ECEF_x(UW_phi, UW_lmbda, UW_h)
UW_y = ECEF_y(UW_phi, UW_lmbda, UW_h)
UW_z = ECEF_z(UW_phi, UW_lmbda, UW_h)
UW_vec = array([UW_x, UW_y, UW_z])
UW_len = norm(UW_vec)

#This loop could be parallelized
for o in O:
    #NOTE: Much of this is probably pretty messy/innefficient
    #I don't have very much experience using pandas and I just want to
    #get it to work.  There isn't that much data so it doesn't matter...

    #Cleanup column names
    o['phi'] = o['Lat (deg)']*pi/180 #Radians
    o['lmbda'] = o['Lon (deg)']*pi/180 #Radians
    o['h'] = 1000*o['Alt (km)'] #Convert to meters
    del o['Lat (deg)']
    del o['Lon (deg)']
    del o['Alt (km)']

    #Convert to ECEF x,y,z
    o['x'] = ECEF_x(o['phi'], o['lmbda'], o['h'])
    o['y'] = ECEF_y(o['phi'], o['lmbda'], o['h'])
    o['z'] = ECEF_z(o['phi'], o['lmbda'], o['h'])

    #Calculate difference between UW and WS
    #this is also the line of sight vector
    o['dist_x'] = o['x'] - UW_x
    o['dist_y'] = o['y'] - UW_y
    o['dist_z'] = o['z'] - UW_z

    #Calculate the L2 norm distance between UW and the satellite
    o['dist'] = o[['dist_x', 'dist_y', 'dist_z']].apply(lambda x: norm(x), 
                                                        axis = 1) #(m)
    
    o['psi_LOS'] = o[['dist_x',
                      'dist_y',
                      'dist_z']].apply(lambda x: pi/2 - arccos(dot(UW_vec, x) /
                                       (UW_len * norm(x))), axis = 1)

    o['LOS'] = o['psi_LOS'] > 1*pi/180 # > deg above horizon

    #Rate of change in distance, each time tick is 60s
    o['d_dist'] = o['dist'].diff()/60. #(m/s)

    #Convert to kilometers
    o['dist_km'] = o['dist']/1000. #(Km)
    o['d_dist_km'] = o['d_dist']/1000. #(Km/s)

def data_throughput(d, LOS, T_step):
    #Calculate the total number of bits successfully sent through the channel
    #At each time step 'd' bps are sent.  The step size is in seconds
    #so we assume constant throughput on each time step and multiply by it.
    return T_step*dot(d, LOS) #bits

#This works but it's stupid
#d1, d2, d3 are the distance ranges I computed
#based on the link margin and sensitivity of the modem.
#Data_rates = [{2.3e5: d1, 2.3e5: d2, 0: d3} for (d1, d2, d3) in 
Data_rates = [{1.2e6: d1, 2.3e5: d2, 19.200e3: d3} for (d1, d2, d3) in 
zip([(0, 1192), (0, 752), (0,532), (0, 474), (0,299), (0, 212)],
    [(1192, 2995), (752, 1890), (532, 1338), (474, 1192), (299, 752), (212, 532)],
    [(2995, 7524), (1890, 4747), (1338, 3361), (1192, 2995), (752, 1890), (532, 1338)])]

labels = ['3.5 - no margin', '3 - no margin', '2 - no margin',
          '3.5 - 8dB margin', '3 - 8dB margin', '2 - 8dB margin']
T_step = 60 #The step time for GTK simulations.

#This does not inlclude FEC rates.
#We can use a LUT to detect signal strength and dynamically configure
#the FEC rate and the data rate through the table.
for rate, label in zip(Data_rates, labels):
    throughput = 0
    for d in rate.keys():
        #Add the data throughput from 
        throughput += data_throughput(d*np.logical_and(
            o['dist_km'] >= rate[d][0],
            o['dist_km'] < rate[d][1]),
                                o['LOS'], T_step)
    print '%s throughput per month: %f Mb' % (label, throughput / 10e6)

for i, o in enumerate(O):
    fig = plt.figure()

    #---Plot Distance---
    ax_dist = fig.add_subplot(2,1,1)
    ax_dist.set_title('%d00Km orbit' % (i + 6))

    #This sets up the colours
    #k: no data - g: turbo - y: fast - r: slow
    cm_dist = ListedColormap(['k', 'g', 'y', 'r'])
    norm_dist = BoundaryNorm([0, 474, 474, 1192, 1192, 2995], cm_dist.N)


    points = array([o.index, o['dist_km']]).T.reshape(-1, 1, 2)
    segments = concatenate([points[:-1], points[1:]], axis = 1)
    lc_dist = LineCollection(segments, cmap = cm_dist, norm = norm_dist)
    lc_dist.set_array(o['LOS']*o['dist_km'])
    ax_dist.add_collection(lc_dist)
    ax_dist.set_xlim(min(o.index), max(o.index))
    ax_dist.set_ylim(min(o['dist_km']), max(o['dist_km']))

    #---Plot angle---
    ax_psi = fig.add_subplot(2, 1, 2)
    cm_LOS = ListedColormap(['k', 'g']) #Color maps
    norm_LOS = BoundaryNorm([-1, 0, 1], cm_LOS.N)
    points = array([o.index, o['psi_LOS']]).T.reshape(-1, 1, 2)
    segments = concatenate([points[:-1], points[1:]], axis = 1)
    lc_psi = LineCollection(segments, cmap = cm_LOS, norm = norm_LOS)
    lc_psi.set_array(2*o['LOS'] - 1)
    ax_psi.add_collection(lc_psi)
    ax_psi.set_xlim(min(o.index), max(o.index))
    ax_psi.set_ylim(min(o['psi_LOS']), max(o['psi_LOS']))

    plt.show()
